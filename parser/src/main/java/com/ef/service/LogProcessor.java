/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ef.service;

import com.ef.model.ParseParameter;

/**
 *
 * @author com4t
 */
public interface LogProcessor {

    public void process(ParseParameter parameter);
}
